//
// Created by gabri on 27/04/2017.
//

#ifndef DIJKSTRA_HOLLOWHEAP_H
#define DIJKSTRA_HOLLOWHEAP_H

#include <vector>

using namespace std;

class Node {
public:
    Node(long i, int k);
    void print();
    long item;
    int key;
    int rank;
    Node *fc;
    Node *ns;
};


class HollowHeap {
public:
    HollowHeap(long max_items);
    bool isEmpty();
    void insert(long e, int k);
    void decreaseKey(long e, int k);
    long deleteMin();

    long insertCount;
    long deleteCount;
    long updateCount;

private:
    Node *makeNode(long e, int k);
    Node *makeChild(Node *w, Node *l);
    Node *link(Node *t1, Node *t2);
    Node *meld(Node *h1, Node *h2);
    void linkHeap(Node *h, vector<Node *> *R);

    Node *root;
    vector<Node *> item_ptrs;
    long size;


};


#endif //DIJKSTRA_HOLLOWHEAP_H
