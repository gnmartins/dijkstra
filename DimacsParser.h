//
// Created by gabri on 27/04/2017.
//

#ifndef DIJKSTRA_DIMACSPARSER_H
#define DIJKSTRA_DIMACSPARSER_H

#include <string>
#include "Graph.h"

Graph dimacsFileToGraph(std::string filename);

#endif //DIJKSTRA_DIMACSPARSER_H
