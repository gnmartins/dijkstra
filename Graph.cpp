//
// Created by gabri on 27/04/2017.
//

#include "Graph.h"
#include <iostream>

Graph::Graph(long n) : arcs(n+1, vector<GraphArc>(1)){
    size = n+1;
}

void Graph::print() {
    for (long i = 0; i < size; ++i) {
        for (long j = 0; j < arcs[i].size(); ++j) {
            std::cout << arcs[i][j].w << " ";
        }
        std::cout << endl;
    }
}

void Graph::setArc(long u, long v, int w) {
    if (u < size && v < size) {
        arcs[u].push_back({v, w});
    }
}

int Graph::getArc(long u, long v) {
    if (u < size && v < arcs[u].size()) {
        return arcs[u][v].w;
    }
    else return -1;
}

Graph createRandomGraph(long n, long m) {
    Graph g(n);
    long s, t;
    int MAX_WEIGHT = 100;
    for (long i = 0; i < m; ++i) {
        s = rand() % g.size;
        t = rand() % g.size;
        s = s==0? 1 : s;
        t = t==0? 1 : t;
        g.setArc(s, t, (rand() % MAX_WEIGHT)+1);
    }

    return g;
}