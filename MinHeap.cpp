//
// Created by gabri on 27/04/2017.
//

#include <cmath>
#include "MinHeap.h"

MinHeap::MinHeap(long max_nodes) : node_ptrs(max_nodes+1, -1) {
    insertCount = 0;
    deleteCount = 0;
    updateCount = 0;
}

bool MinHeap::isEmpty() {
    if (list.size() > 0) {
        return false;
    }
    else {
        return true;
    }
}

void MinHeap::insert(long u, int key) {
    list.push_back({u, key});
    long u_pos = list.size()-1;
    node_ptrs[u] = u_pos;
    heapifyUp(u_pos);

    insertCount++;
}

long MinHeap::deleteMin() {
    long last_pos = list.size()-1;
    swap(0, last_pos);
    MinHeapNode min = list.back();
    list.pop_back();
    heapifyDown(0);

    deleteCount++;
    return min.node;
}

void MinHeap::decreaseKey(long u, int key) {
    long u_ptr = node_ptrs[u];
    int old_key = list[u_ptr].key;

    if (key < old_key) {
        list[u_ptr] = {u, key};
        heapifyUp(u_ptr);
    }

    updateCount++;
}

long MinHeap::left(long i) {
    return 2*i + 1;
}

long MinHeap::right(long i) {
    return 2*i + 2;
}

long MinHeap::parent(long i) {
    return (long) floor((i-1)/2);
}

long MinHeap::key(long i) {
    return list[i].key;
}

int MinHeap::numberOfChildren(long i) {
    if (right(i) < list.size()) return 2;
    if (left(i) < list.size()) return 1;
    return 0;
}

void MinHeap::swap(long i, long j) {
    MinHeapNode temp = list[i];
    list[i] = list[j];
    list[j] = temp;
    node_ptrs[ list[i].node ] = i;
    node_ptrs[ list[j].node ] = j;
}

bool MinHeap::root(long i) {
    return i == 0;
}

void MinHeap::heapifyUp(long i) {
    if (root(i)) return;
    if (key(parent(i)) > key(i)) {
        swap(parent(i), i);
        heapifyUp(parent(i));
    }
}

void MinHeap::heapifyDown(long i) {
    switch (numberOfChildren(i)) {
        case 0:
            break;
        case 1:
            if (key(left(i)) < key(i))
                swap(left(i), i);
            break;
        case 2:
            if (key(left(i)) < key(right(i))) {
                swap(left(i), i);
                heapifyDown(left(i));
            } else {
                swap(right(i), i);
                heapifyDown(right(i));
            }
            break;
        default:
            break;
    }
}


