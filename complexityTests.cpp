//
// Created by gabri on 29/04/2017.
//

#include "complexityTests.h"
#include <iostream>
#include <cmath>
#include <chrono>
#include <numeric>

#include "Heap.h"
#include "Graph.h"
#include "dijkstra.h"

using namespace std;

long randNode(long max) {
    long r = rand() % max;
    if (r == 0) r = 1;
    return r;
}

/* Heap tests */
void insertTest(int heap_type) {
    cout << "\t[insert test]" << endl;
    long n = pow(2, 20) - 1;
    Heap heap(n, heap_type);

    vector<long> n_inserts;
    vector<long> elapsed_time;

    long u = 0, inserts = 0;
    auto start = chrono::system_clock::now();
    for (long i = 1; i <= n; ++i) {
        if (inserts == pow(2, u)-1) {
            auto end = chrono::system_clock::now();
            auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);
            if (u > 0) elapsed_time.push_back(elapsed.count());
            if (u > 0) n_inserts.push_back(pow(2,u)-1 - pow(2,u-1));
            u++;
            start = chrono::system_clock::now();
        }

        heap.insert(i, n-i);
        inserts++;
    }

    if (inserts == pow(2, u)-1) {
        auto end = chrono::system_clock::now();
        auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);
        elapsed_time.push_back(elapsed.count());
        n_inserts.push_back(pow(2,u)-1 - pow(2,u-1));
    }

    cout << "insertions\ttime(microseconds)" << endl;
    for (int j = 1; j < elapsed_time.size(); ++j) {
        cout << n_inserts[j] << "\t" << elapsed_time[j] << endl;
    }
}

void updateTest(int heap_type) {
    cout << "\t[update test]" << endl;
    cout << "updates\ttime(microseconds)" << endl;
    for (int i = 1; i <= 20; ++i) {
        long n = pow(2,i)-1 + pow(2,i);
        Heap heap(n, heap_type);

        for (int k = 1; k <= pow(2,i)-1; ++k) {
            heap.insert(k, pow(2, i)+1);
        }
        for (int k = pow(2,i); k <= n; ++k) {
            heap.insert(k, pow(2, i)+2);
        }
        long dec = 0;
        auto start = chrono::system_clock::now();
        for (int k = pow(2,i); k <= n; ++k) {
            heap.decreaseKey(k, pow(2,i)-dec);
            dec++;
        }
        auto end = chrono::system_clock::now();
        auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);

        cout << pow(2,i) << "\t" << elapsed.count() << endl;
    }
}

void deleteTest(int heap_type) {
    cout << "\t[delete test]" << endl;
    cout << "deletes\ttime(microseconds)" << endl;
    for (int i = 1; i <= 20; ++i) {
        long n = pow(2,i)-1;
        Heap heap(n, heap_type);

        for (int k = 1; k <= n; ++k) {
            heap.insert(k, randNode(n));
        }

        auto start = chrono::system_clock::now();
        for (int k = 1; k <= n; ++k) {
            heap.deleteMin();
        }

        auto end = chrono::system_clock::now();
        auto elapsed = chrono::duration_cast<chrono::microseconds>(end - start);

        cout << i << "\t" << elapsed.count() << endl;
    }
}

void heapTest() {
    cout << "--- Hollow heap test ---" << endl;
    insertTest(Heap::HOLLOWHEAP);
    updateTest(Heap::HOLLOWHEAP);
    deleteTest(Heap::HOLLOWHEAP);
    cout << "------------------------" << endl;
}

/* Dijkstra tests */
void fixedNTest(int heap_type) {
    long src, tgt;
    cout << "\t[fixed n]" << endl;
    cout << "n \t\tm \t\ttime(ms)" << endl;
    long n = pow(2, 12), m;

    for (int i = 13; i <= 22; ++i) {
        m = pow(2, i);
        vector<long> elapsed_time;

        Graph g = createRandomGraph(n, m);
        for (int k = 0; k < 30; ++k) {
            src = randNode(g.size);
            tgt = src;
            while (src == tgt) tgt = randNode(g.size);

            auto start = chrono::system_clock::now();
            dijkstra(src, tgt, &g, heap_type);
            auto end = chrono::system_clock::now();
            auto elapsed = chrono::duration_cast<chrono::milliseconds>(end - start);
            elapsed_time.push_back(elapsed.count());
        }
        double avg_time = accumulate(elapsed_time.begin(), elapsed_time.end(), 0.0)/elapsed_time.size();


        cout << n << "\t" << m << "\t" << avg_time << endl;
    }
}

void fixedMTest(int heap_type) {
    long src, tgt;
    cout << "\t[fixed m]" << endl;
    cout << "n \t\tm \t\ttime(ms)" << endl;
    long n, m = pow(2, 20);

    for (int i = 11; i <= 20; ++i) {
        n = pow(2, i);
        vector<long> elapsed_time;
        Graph g = createRandomGraph(n, m);
        for (int k = 0; k < 30; ++k) {
            src = randNode(g.size);
            tgt = src;
            while (src == tgt) tgt = randNode(g.size);

            auto start = chrono::system_clock::now();
            dijkstra(src, tgt, &g, heap_type);
            auto end = chrono::system_clock::now();
            auto elapsed = chrono::duration_cast<chrono::milliseconds>(end - start);
            elapsed_time.push_back(elapsed.count());
        }
        double avg_time = accumulate(elapsed_time.begin(), elapsed_time.end(), 0.0)/elapsed_time.size();
        cout << n << "\t" << m << "\t" << avg_time << endl;
    }
}

void dijkstraTest() {
    cout << "--- Dijkstra test ---" << endl;
    cout << "Binary heap test: " << endl;
    fixedNTest(Heap::MINHEAP);
    fixedMTest(Heap::MINHEAP);
    cout << endl << endl;
    cout << "Hollow heap test: " << endl;
    fixedNTest(Heap::HOLLOWHEAP);
    fixedMTest(Heap::HOLLOWHEAP);
    cout << "---------------------" << endl;
}
