#include <iostream>
#include <chrono>
#include <cstring>
#include <cassert>

#include "Graph.h"
#include "dimacsParser.h"
#include "dijkstra.h"
#include "complexityTests.h"

using namespace std;

int main(int argc, char *argv[]) {

    assert(argc >= 4);
    long src = stol(argv[1]);
    long tgt = stol(argv[2]);
    std::string filename = argv[3];
    bool test = false, runs = false;
    int heap_type = Heap::HOLLOWHEAP;

    //region arg parsing
    for (int i = 4; i < argc; ++i) {
        if (strcmp(argv[i], "--test") == 0
            || strcmp(argv[i], "-t") == 0) test = true;
        else
        if (strcmp(argv[i], "--binary") == 0
            || strcmp(argv[i], "-b") == 0) heap_type = Heap::MINHEAP;
        else
        if (strcmp(argv[i], "--hollow") == 0
            || strcmp(argv[i], "-h") == 0) heap_type = Heap::HOLLOWHEAP;
        else
        if (strcmp(argv[i], "--30runs") == 0
            || strcmp(argv[i], "-r") == 0) runs = true;
    }
    //endregion

    if (!test) {
        auto start = chrono::system_clock::now();
        Graph g = dimacsFileToGraph(filename);
        auto end = chrono::system_clock::now();
        auto elapsed = chrono::duration_cast<chrono::milliseconds>(end - start);
        cout << "\t[dimacs parsing] elapsed time: " << elapsed.count() << "ms" << endl;
        int max_it = 1, distance;
        if (runs) max_it = 30;
        for (int i = 0; i < max_it; ++i) {
            if (runs) {
                src = randNode(g.size);
                tgt = src;
                while (tgt == src) tgt = randNode(g.size);
            }

            start = chrono::system_clock::now();
            distance = dijkstra(src, tgt, &g, heap_type);
            end = chrono::system_clock::now();
            elapsed = chrono::duration_cast<chrono::milliseconds>(end - start);
            cout << "\t[dijkstra] elapsed time: " << elapsed.count() << "ms" << endl;

        }
        std::string dist = distance != INT32_MAX ? std::to_string(distance) : "inf";
        std::cout << dist << std::endl;
    }
    else {
        heapTest();
        dijkstraTest();
    }

    return 0;
}