//
// Created by gabri on 27/04/2017.
//

#ifndef DIJKSTRA_MINHEAP_H
#define DIJKSTRA_MINHEAP_H

#include <vector>

using namespace std;

typedef struct node {
    long node;
    int key;
} MinHeapNode;

class MinHeap {
public:
    MinHeap(long max_nodes);
    bool isEmpty();
    void insert(long u, int key);
    long deleteMin();
    void decreaseKey(long u, int key);

    long insertCount;
    long deleteCount;
    long updateCount;

private:
    vector<MinHeapNode> list;
    vector<long> node_ptrs;

    long left(long i);
    long right(long i);
    long parent(long i);
    long key(long i);
    int numberOfChildren(long i);
    void swap(long i, long j);
    bool root(long i);
    void heapifyUp(long i);
    void heapifyDown(long i);
};


#endif //DIJKSTRA_MINHEAP_H
