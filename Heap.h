//
// Created by gabri on 29/04/2017.
//

#ifndef DIJKSTRA_HEAP_H
#define DIJKSTRA_HEAP_H


#include "MinHeap.h"
#include "HollowHeap.h"

class Heap {
public:
    Heap(long max_size, int heap_type);
    bool isEmpty();
    void insert(long u, int key);
    long deleteMin();
    void decreaseKey(long u, int key);

    long getInsertCount();
    long getDeleteCount();
    long getUpdateCount();

    static const int MINHEAP = 0;
    static const int HOLLOWHEAP = 1;

private:
    int type;

    MinHeap *min_heap;
    HollowHeap *hollow_heap;
};


#endif //DIJKSTRA_HEAP_H
