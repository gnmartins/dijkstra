//
// Created by gabri on 29/04/2017.
//

#ifndef DIJKSTRA_COMPLEXITYTESTS_H
#define DIJKSTRA_COMPLEXITYTESTS_H

long randNode(long max);
void heapTest();
void dijkstraTest();

#endif //DIJKSTRA_COMPLEXITYTESTS_H
