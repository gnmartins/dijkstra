//
// Created by gabri on 27/04/2017.
//

#include <iostream>
#include "HollowHeap.h"

Node::Node(long i, int k) {
    item = i;
    key = k;
    rank = 0;
    fc = nullptr;
    ns = nullptr;
}

void Node::print() {
    cout << "item: " << item << endl;
    cout << "key: " << key << endl;
    cout << "rank: " << rank << endl;

    cout << "fc: ";
    if (fc != nullptr) {
        cout << "(" << fc->item << ", " << fc->key << ")" << endl;
    }
    else {
        cout << "null" << endl;
    }

    cout << "ns: ";
    if (ns != nullptr) {
        cout << "(" << ns->item << ", " << ns->key << ")" << endl;
    }
    else {
        cout << "null" << endl;
    }
}

HollowHeap::HollowHeap(long max_items) : item_ptrs(max_items+1, nullptr) {
    root = nullptr;
    size = 0;

    insertCount = 0;
    deleteCount = 0;
    updateCount = 0;
}

bool HollowHeap::isEmpty() {
    if (root != nullptr) {
        return false;
    }
    else {
        return true;
    }
}

Node *HollowHeap::makeChild(Node *w, Node *l) {
    l->ns = w->fc;
    w->fc = l;
    w->rank = w->rank + 1;
    return w;
}

Node *HollowHeap::link(Node *t1, Node *t2) {
    if (t1->key <= t2->key) {
        return makeChild(t1, t2);
    }
    else {
        return makeChild(t2, t1);
    }
}

Node *HollowHeap::makeNode(long e, int k) {
    Node* u = new Node(e, k);
    u->ns = u;
    item_ptrs[e] = u;
    return u;
}

Node *HollowHeap::meld(Node *h1, Node *h2) {
    if (h1 == nullptr) return h2;
    if (h2 == nullptr) return h1;

    Node *aux = h1->ns;
    h1->ns = h2->ns;
    h2->ns = aux;

    if (h1->key <= h2->key) {
        return h1;
    }
    else {
        return h2;
    }
}

void HollowHeap::insert(long e, int k) {
    root = meld(makeNode(e, k), root);

    insertCount++;
}

void HollowHeap::decreaseKey(long e, int k) {
    Node* u = item_ptrs[e];
    u->item = -1;
    Node* v = makeNode(e, k);
    v->rank = max(0, u->rank-2);

    if (u->rank >= 2) {
        v->fc = u->fc->ns->ns;
        u->fc->ns->ns = nullptr;
    }

    root = meld(v, root);

    updateCount++;
}

long HollowHeap::deleteMin() {
    if (root == nullptr) return -1;
    // root holds the min item
    long min_item = root->item;

    // min_item will be removed, so its pointer must become null
    item_ptrs[min_item] = nullptr;

    root->item = -1;
    vector<Node*> R;

    Node *r = root;
    Node *rn;
    do {
        rn = r->ns;
        linkHeap(r, &R);
        r = rn;
    } while (r != root);

    root = nullptr;
    for (int i = 0; i < R.size(); ++i) {
        if (R[i] != nullptr) {
            R[i]->ns = R[i];
            root = meld(root, R[i]);
        }
    }

    deleteCount++;
    return min_item;
}

void HollowHeap::linkHeap(Node *h, vector<Node *> *R) {
    //if h is hollow
    if (h->item == -1) {
        Node *r = h->fc;
        while (r != nullptr) {
            Node *rn = r->ns;
            linkHeap(r, R);
            r = rn;
        }
        delete h;
    }
    else {
        int i = h->rank;
        while (R->size() <= i) R->push_back(nullptr);

        while (R->at(i) != nullptr) {
            h = link(h, R->at(i));
            R->at(i) = nullptr;

            i = i + 1;
            while (R->size() <= i) R->push_back(nullptr);
        }
        R->at(i) = h;
    }
}
