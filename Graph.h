//
// Created by gabri on 27/04/2017.
//

#ifndef DIJKSTRA_GRAPH_H
#define DIJKSTRA_GRAPH_H

#include <vector>

using namespace std;

typedef struct GraphArc {
    long tl;
    int w;
} GraphArc;


class Graph {
public:
    Graph(long n);
    void print();
    void setArc(long u, long v, int w);
    int getArc(long u, long v);

    vector<vector<GraphArc>> arcs;
    long size;
};

Graph createRandomGraph(long n, long m);

#endif //DIJKSTRA_GRAPH_H
