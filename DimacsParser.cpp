//
// Created by gabri on 27/04/2017.
//

#include "dimacsParser.h"

#include <fstream>
#include <iostream>
#include <sstream>

Graph dimacsFileToGraph(std::string filename) {
    std::string line = "", dummy;
    std::ifstream file(filename);
    long n, m;
    if (file.is_open()) {
        while (line.substr(0, 4) != "p sp")
            getline(file, line);

        std::stringstream linestr;
        linestr.str(line);
        linestr >> dummy >> dummy >> n >> m;
        Graph g(n);
        long i = 0;
        while (i < m) {
            getline(file, line);
            std::stringstream arc(line);
            long u, v;
            int w;
            char ac;
            arc >> ac >> u >> v >> w;

            g.setArc(u, v, w);
            i++;
        }
        return g;
    }
    else {
        std::cout << "Unable to open file " << filename << endl;
        return Graph(0);
    }
}
