//
// Created by gabri on 29/04/2017.
//

#include "Heap.h"

Heap::Heap(long max_size, int heap_type) {
    type = heap_type;

    switch (type) {
        case MINHEAP:
            min_heap = new MinHeap(max_size);
            hollow_heap = nullptr;
            break;
        case HOLLOWHEAP:
            min_heap = nullptr;
            hollow_heap = new HollowHeap(max_size);
            break;
        default:
            min_heap = new MinHeap(max_size);
            hollow_heap = nullptr;
    }
}

bool Heap::isEmpty() {
    switch (type) {
        case MINHEAP:
            return min_heap->isEmpty();
        case HOLLOWHEAP:
            return hollow_heap->isEmpty();
        default:
            return min_heap->isEmpty();
    }
}

void Heap::insert(long u, int key) {
    switch (type) {
        case MINHEAP:
            return min_heap->insert(u, key);
        case HOLLOWHEAP:
            return hollow_heap->insert(u, key);
        default:
            return min_heap->insert(u, key);
    }
}

long Heap::deleteMin() {
    switch (type) {
        case MINHEAP:
            return min_heap->deleteMin();
        case HOLLOWHEAP:
            return hollow_heap->deleteMin();
        default:
            return min_heap->deleteMin();
    }
}

void Heap::decreaseKey(long u, int key) {
    switch (type) {
        case MINHEAP:
            return min_heap->decreaseKey(u, key);
        case HOLLOWHEAP:
            return hollow_heap->decreaseKey(u, key);
        default:
            return min_heap->decreaseKey(u, key);
    }
}

long Heap::getInsertCount() {
    switch (type) {
        case MINHEAP:
            return min_heap->insertCount;
        case HOLLOWHEAP:
            return hollow_heap->insertCount;
        default:
            return min_heap->insertCount;
    }
}

long Heap::getDeleteCount() {
    switch (type) {
        case MINHEAP:
            return min_heap->deleteCount;
        case HOLLOWHEAP:
            return hollow_heap->deleteCount;
        default:
            return min_heap->deleteCount;
    }
}

long Heap::getUpdateCount() {
    switch (type) {
        case MINHEAP:
            return min_heap->updateCount;
        case HOLLOWHEAP:
            return hollow_heap->updateCount;
        default:
            return min_heap->updateCount;
    }
}
