//
// Created by gabri on 27/04/2017.
//

#include "dijkstra.h"
#include <cstdint>
#include <numeric>
#include <iostream>

using namespace std;

int dijkstra(long src, long tgt, Graph *g, int heap_type) {

    vector<int> d(g->size + 1);
    for (long v = 1; v < d.size(); ++v) {
        d[v] = v != src ? INT32_MAX : 0;
    }

    vector<bool> visited(g->size + 1);
    for (long v = 1; v < visited.size(); ++v) {
        visited[v] = false;
    }

    Heap Q(g->size, heap_type);

    Q.insert(src, 0);
    while (!Q.isEmpty()) {
        long v = Q.deleteMin();
        visited[v] = true;
        for (long neighbor = 0; neighbor < g->arcs[v].size(); ++neighbor) {
            long u = g->arcs[v][neighbor].tl;
            if (!visited[u]) {
                if (d[u] == INT32_MAX) {
                    d[u] = d[v] + g->getArc(v, neighbor);
                    Q.insert(u, d[u]);
                } else if (d[v] + g->getArc(v, neighbor) < d[u]) {
                    d[u] = d[v] + g->getArc(v, neighbor);
                    Q.decreaseKey(u, d[u]);
                }
            }
        }
    }

    return d[tgt];
}