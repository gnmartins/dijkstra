//
// Created by gabri on 27/04/2017.
//

#ifndef DIJKSTRA_DIJKSTRA_H
#define DIJKSTRA_DIJKSTRA_H


#include "Graph.h"
#include "Heap.h"

int dijkstra(long src, long tgt, Graph* g, int heap_type);

#endif //DIJKSTRA_DIJKSTRA_H
